# API STARTER NODE / EXPRESS / MONGO

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm start
```
### Compiles and hot-reloads for development
```
npm start
```
### Config for db & server
```
create .env file "/"
add PORT = {Your_PORT}, MONGODB_URL{YOUR_MONGODBCOMPASS_LINK}, JWT_SECRET{RANDOM_API_KEY}
```
### For more details on expressjs
See [Documentation](https://expressjs.com/routing.html).